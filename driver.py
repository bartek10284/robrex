import canopen

class Driver:  
    
    
    def __init__(self, node,dic_dir,network,**kwargs):
      
        self.node = network.add_node(node,dic_dir)         
        self.node.sdo['Max Profile Velocity'].write(1500)
        self.node.sdo['Profile Acceleration'].write(1000)
        self.node.sdo['Profile Deceleration'].write(1000)
        self.node.sdo['QuickStop Deceleration'].write(1000)
        self.node.sdo['Motion Profile Type'].write(0)
        self.node.sdo['Motor Data']['Output Current Limit'].write(4000)
        
     
        self.Modes = self.node.sdo['Modes of Operation']
        self.ControlWord =self.node.sdo['Controlword']         
        self.TargetVelocity = self.node.sdo['Target Velocity']       
        self.AccualCurent = self.node.sdo['Current Actual Value'].phys      
        self.TargetCurrent = self.node.sdo['Current Mode Setting Value']
        self.TargetPosition = self.node.sdo['Target Position']
    
    def velocityMode(self):
        self.node.nmt.send_command(0x1)
        self.Modes.write(0x03)
        self.ControlWord.write(0x0006)
         
    def curentMode(self):
        self.node.nmt.send_command(0x1)
        self.Modes.write(-0x03)
        self.ControlWord.write(0x0006)
        self.ControlWord.write(0x000F)
        pass    
    
    def setVelocity(self,target):
        self.ControlWord.write(0x000F)
        self.TargetVelocity.write(target)    
    
    def stop(self):
        self.ControlWord.write(0x000F)
        self.TargetVelocity.write(0)
        self.TargetCurrent.write(0)
        self.ControlWord.write(0x0006)
    
    def homing(self):
        self.ControlWord.write(0x0006)
        self.node.nmt.send_command(0x1)
        self.Modes.write(0x06)
        self.node.sdo['Max Following Error'].write(10000)
        self.node.sdo['Max Profile Velocity'].write(1500)
        self.node.sdo['Homing Speeds'][1].write(100)
        self.node.sdo['Homing Speeds'][2].write(10)
        self.node.sdo['Home Offset'].write(0)
        self.node.sdo['QuickStop Deceleration'].write(10000)
        self.node.sdo['Homing Acceleration'].write(1000)
        self.node.sdo['Current Threshold for Homing Mode'].write(500)
        self.node.sdo['Home Position'].write(0)
       
       
       
        self.node.sdo['Homing Method'].write(0x23)        
        self.ControlWord.write(0x0006)
        self.ControlWord.write(0x000F)
        
        self.ControlWord.write(0x000F)        
        self.ControlWord.write(0x001F)
        
    def positionMode(self):
        
        self.node.nmt.send_command(0x1)
        self.node.sdo['Max Following Error'].write(10000)
        self.Modes.write(0x01)        
        self.ControlWord.write(0x0006)
        self.ControlWord.write(0x000F)
        
            
        
    
    def setPosition(self,target):
        self.TargetPosition.write(target)
        self.ControlWord.write(0x001F)
    
    
    def setCurtent(self,target):
        self.TargetCurrent.write(target)
        pass
    
    def modeget(self):
        print(self.Modes.read()) 
    
    