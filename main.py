'''
Created on 7 sie 2018

@author: Bernaciak Bartlomiej

'''
from driver import Driver

import numpy as np
import canopen

import pygame, sys, time    #Imports Modules
from pygame.locals import *

pygame.init()#Initializes Pygame
pygame.joystick.init()
joystick = pygame.joystick.Joystick(1)
joystick.init()#Initializes Joystick


network = canopen.Network()
network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=500000)

silnik = Driver(5,'node5.dcf',network)
silnik2 = Driver(6,'node6.dcf',network)
silnik3 = Driver(7,'node7.dcf',network)
silnik4 = Driver(8,'node8.dcf',network)
silnik5 = Driver(9,'node9.dcf',network)
silnik6 = Driver(10,'node10.dcf',network)




silnik.velocityMode()
silnik2.positionMode()
silnik3.velocityMode()
silnik4.curentMode()
silnik5.curentMode()

silnik6.velocityMode()


brake=True 

network.send_message(0x000,[0x01,0x20])
network.send_message(0x620, [0x2F,0x50,0x22,0x01,0x01,0x00,0x00,0x00])

ones = True

axies = [0,0,0,0,0]
pos = 0 
while True: 
    print(silnik2.node.sdo['Position Actual Value'].phys)
    status = silnik2.node.sdo['Statusword'].bits[10]
    for event in pygame.event.get():
        if event.type == QUIT:
            loopQuit = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                loopQuit = True     
    
    for i in range(0,4):
        axis = joystick.get_axis(i)
        if axis <0.1 and axis > -0.1:
            axis = 0
        axies[i] = axis
    
   
    pos = pos+axies[1]*500
    silnik.setVelocity(axies[0]*100)
    if ones:
        silnik2.setPosition(pos)
    silnik3.setVelocity(axies[2]*100)
    
    if not brake:
        silnik6.setVelocity(axies[3]*800)   
    if joystick.get_button(4):
        network.send_message(0x620, [0x2F,0x60,0x22,0x01,0x01,0x00,0x00,0x00])
        brake = False
    elif joystick.get_button(5):
        network.send_message(0x620, [0x2F,0x60,0x22,0x01,0x00,0x00,0x00,0x00])
        brake = True
    if joystick.get_button(1): 
        silnik4.setCurtent(100)
    
    elif joystick.get_button(0):
        silnik4.setCurtent(-100)
    else:
        silnik4.setCurtent(0)
   
    if joystick.get_button(2): 
        silnik5.setCurtent(100)    
    elif joystick.get_button(3):
        silnik5.setCurtent(-100)
    else:
        silnik5.setCurtent(0)
    
    if joystick.get_button(6) and ones:
        ones = False
        silnik2.homing()
        pos = 0
        print('homing')
        time.sleep(1)
        ones = True
        silnik2.positionMode()
 
    if joystick.get_button(7):       
        silnik5.stop()
        silnik3.stop()
        silnik.stop()
        silnik2.stop()
        break

network.disconnect()
pygame.quit()
sys.exit()



